package first.global.fgcscore.Utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class SaveData {

    Context ctx;
    SharedPreferences preferences;
    SharedPreferences.Editor editor;

    public SaveData(Context ctx1) {
        this.ctx = ctx1;
        preferences = PreferenceManager.getDefaultSharedPreferences(ctx);
        editor = preferences.edit();

    }


    public String getScore() {

        return preferences.getString("order_id", "0");
    }

    public void saveScore(String score) {
        editor.putString("order_id",score);
        editor.commit();
    }

    public String get_list_score() {

        return preferences.getString("score_list", "");
    }

    public void save_list_score(String score) {
        editor.putString("score_list",score);
        editor.commit();
    }
    public Boolean isguest() {

        return preferences.getBoolean("guest_mode", true);
    }

    public void setguest(Boolean first_time) {
        editor.putBoolean("guest_mode",first_time);
        editor.commit();
    }


    public void saveuserinfo(String Country_Name,String Country_Username){

        editor.putString("country_name",Country_Name);
        editor.putString("country_username",Country_Username);
        editor.commit();
    }

    public String get_country_name() {

        return preferences.getString("country_name", "");
    }
    public String get_country_username() {

        return preferences.getString("country_username", "");
    }


    public void clearAll() {
        editor.clear();
        editor.commit();
    }
}