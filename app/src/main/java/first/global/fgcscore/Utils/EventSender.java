package first.global.fgcscore.Utils;

public class EventSender {
    public String event;

    public EventSender(String event) {
        this.event = event;
    }

    public String getEvent() {
        return event;
    }

    public void setEvent(String event) {
        this.event = event;
    }
}
