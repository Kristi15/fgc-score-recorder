package first.global.fgcscore.Fragment;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.codehaus.jackson.map.DeserializationConfig;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import first.global.fgcscore.Activities.ScoreListActivity;
import first.global.fgcscore.Adapter.ScoreAdapter;
import first.global.fgcscore.Model.ScoreTeamModel;
import first.global.fgcscore.R;
import first.global.fgcscore.Utils.SaveData;

public class Fragment_Local_ScoreBoard extends Fragment {
    RecyclerView score_recycler;
    List<ScoreTeamModel> feed_item_list= new ArrayList<>();
    ScoreAdapter adapter;
    RelativeLayout emty_relative;
    LinearLayout table_row;
    Gson gson;
    SaveData saveData;
    ScoreTeamModel test = new ScoreTeamModel();
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_fragment__local__score_board, container, false);


        saveData = new SaveData(getActivity());
        score_recycler = (RecyclerView) v.findViewById(R.id.score_list);
        emty_relative = (RelativeLayout) v.findViewById(R.id.emty_relative);
        table_row = (LinearLayout) v.findViewById(R.id.table_row);

        gson = new GsonBuilder().create();
        fill_by_id();
        return  v;

    }

    public void fill_by_id() {
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(DeserializationConfig.Feature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true);
        mapper.configure(DeserializationConfig.Feature.FAIL_ON_UNKNOWN_PROPERTIES, false);

        try {
            feed_item_list = mapper.readValue(saveData.get_list_score(), new TypeReference<List<ScoreTeamModel>>() {
            });

        } catch (IOException e) {
            e.printStackTrace();

        }

        System.out.println("listtt is" + saveData.get_list_score());

        adapter = new ScoreAdapter(getActivity(), getActivity(), feed_item_list);

        score_recycler.setLayoutManager(new GridLayoutManager(getActivity(), 1));

        score_recycler.setAdapter(adapter);

        adapter.notifyDataSetChanged();
        if (feed_item_list.size() == 0) {
            emty_relative.setVisibility(View.VISIBLE);
            table_row.setVisibility(View.GONE);
        } else {
            table_row.setVisibility(View.VISIBLE);
            emty_relative.setVisibility(View.GONE);
        }


    }
}