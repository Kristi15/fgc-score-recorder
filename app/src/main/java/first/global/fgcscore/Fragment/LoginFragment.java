package first.global.fgcscore.Fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Credentials;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.telecom.Call;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.greenrobot.eventbus.EventBus;

import first.global.fgcscore.MainActivity;
import first.global.fgcscore.Model.GlobalScoreModel;
import first.global.fgcscore.Model.SignInModel;
import first.global.fgcscore.R;
import first.global.fgcscore.Rest.API;
import first.global.fgcscore.Rest.ApiClient;
import first.global.fgcscore.Utils.EventSender;
import first.global.fgcscore.Utils.SaveData;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginFragment extends Fragment {
    EditText username_et,password_et;
    Button login_btn;
    Gson gson;
    SaveData saveData;
    ProgressDialog dialog;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v =  inflater.inflate(R.layout.fragment_login, container, false);
        initwidgest(v);



        return v;
    }

    private void initwidgest(View v) {
        username_et = (EditText) v.findViewById(R.id.username_et);
        password_et = (EditText) v.findViewById(R.id.password_et);
        login_btn = (Button) v.findViewById(R.id.login_btn);
        gson = new GsonBuilder().create();
        saveData = new SaveData(getActivity());


        login_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!username_et.getText().toString().equalsIgnoreCase("")
                        && !password_et.getText().toString().equalsIgnoreCase("")) {
                    callforlogin(username_et.getText().toString(), password_et.getText().toString());

                }

                if (username_et.getText().toString().equalsIgnoreCase("")){
                    YoYo.with(Techniques.Tada)
                            .duration(700)
                            .repeat(0)
                            .playOn(username_et);

                }
                if (password_et.getText().toString().equalsIgnoreCase("")){
                    YoYo.with(Techniques.Tada)
                            .duration(700)
                            .repeat(0)
                            .playOn(password_et);

                }

            }
        });



    }
    public void loading(){
        dialog = new ProgressDialog(getActivity());
        dialog.setTitle("Please Wait!");
        dialog.setMessage("We are checking your credentials!");
        dialog.show();
    }






    public void callforlogin(String username,String password){
        loading();
        API apiClient = ApiClient.createAPI_No_Token();
        retrofit2.Call<SignInModel> call = apiClient.login(username,password);
        call.enqueue(new Callback<SignInModel>() {
            @Override
            public void onResponse(retrofit2.Call<SignInModel> call, Response<SignInModel> response) {

                if (!gson.toJson(response.body()).equalsIgnoreCase("null")){
                    if (!gson.toJson(response.body().getMessage()).equalsIgnoreCase("fail")){

                        saveData.setguest(false);
                        saveData.saveuserinfo(response.body().getData().getName(),response.body().getData().getCountryUsername());
                        get_socre_byname(response.body().getData().getName());
                    }else{
                        Toast.makeText(getActivity(), "Credentials are Wrong!", Toast.LENGTH_SHORT).show();

                    }
                }else{
                    Toast.makeText(getActivity(), "Credentials are Wrong!", Toast.LENGTH_SHORT).show();

                }
            }

            @Override
            public void onFailure(retrofit2.Call<SignInModel> call, Throwable t) {
                Toast.makeText(getActivity(), "Credentials are Wrong!", Toast.LENGTH_SHORT).show();
                dialog.dismiss();
            }
        });
    }
     public  void get_socre_byname(final String name){
        API apiCLient =ApiClient.createAPI_No_Token();
        retrofit2.Call<GlobalScoreModel> call = apiCLient.get_globalScores();
        call.enqueue(new Callback<GlobalScoreModel>() {
            @Override
            public void onResponse(retrofit2.Call<GlobalScoreModel> call, Response<GlobalScoreModel> response) {
                dialog.dismiss();

                if (!gson.toJson(response.body()).equalsIgnoreCase("null")){


                    System.out.println("respone json " + gson.toJson(response.body()));
                    for (int i = 0; i<response.body().getData().size();i++){
                        if (name.equalsIgnoreCase(response.body().getData().get(i).getCountryName())) {
                            saveData.saveScore(response.body().getData().get(i).getCountryScore());
                            i = response.body().getData().size();
                        }else{
                            saveData.saveScore("0");
                        }

                    }

                    EventBus.getDefault().post(new EventSender("new_score%"+saveData.getScore()));
                    startActivity(new Intent(getActivity(),MainActivity.class));



                }else{
                    startActivity(new Intent(getActivity(),MainActivity.class));


                }
            }

            @Override
            public void onFailure(retrofit2.Call<GlobalScoreModel> call, Throwable t) {
                dialog.dismiss();

                startActivity(new Intent(getActivity(),MainActivity.class));
            }
        });

     }

}
