package first.global.fgcscore.Fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.telecom.Call;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.ArrayList;
import java.util.List;

import first.global.fgcscore.Adapter.GlobalScoreAdapter;
import first.global.fgcscore.Adapter.ScoreAdapter;
import first.global.fgcscore.Model.Data.GlobalScoreDataModel;
import first.global.fgcscore.Model.GlobalScoreModel;
import first.global.fgcscore.Model.ScoreTeamModel;
import first.global.fgcscore.R;
import first.global.fgcscore.Rest.API;
import first.global.fgcscore.Rest.ApiClient;
import first.global.fgcscore.Utils.SaveData;
import retrofit2.Callback;
import retrofit2.Response;

public class Fragment_Global_ScoreBoard extends Fragment {
    RecyclerView score_recycler;
    List<GlobalScoreDataModel> feed_item_list= new ArrayList<>();
    GlobalScoreAdapter adapter;
    RelativeLayout emty_relative;
    LinearLayout table_row;
    Gson gson;
    SaveData saveData;
    ScoreTeamModel test = new ScoreTeamModel();
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_fragment__global__score_board, container, false);

        saveData = new SaveData(getActivity());
        score_recycler = (RecyclerView) v.findViewById(R.id.score_list);
        emty_relative = (RelativeLayout) v.findViewById(R.id.emty_relative);
        table_row = (LinearLayout) v.findViewById(R.id.table_row);

        gson = new GsonBuilder().create();
        score_recycler.setLayoutManager(new GridLayoutManager(getActivity(),1));
        adapter = new GlobalScoreAdapter(getActivity(),getActivity(),feed_item_list);


        fill_from_call();



        return v;
    }

    private void fill_from_call() {
        API apiClient = new ApiClient().createAPI_No_Token();
        retrofit2.Call<GlobalScoreModel> call = apiClient.get_globalScores();
        call.enqueue(new Callback<GlobalScoreModel>() {
            @Override
            public void onResponse(retrofit2.Call<GlobalScoreModel> call, Response<GlobalScoreModel> response) {
                if (!gson.toJson(response.body()).equalsIgnoreCase("null")){
                    feed_item_list  = response.body().getData();
                    adapter = new GlobalScoreAdapter(getActivity(),getActivity(),feed_item_list);
                    score_recycler.setAdapter(adapter);
                    adapter.notifyDataSetChanged();
                    if (feed_item_list.size()<1){
                        emty_relative.setVisibility(View.VISIBLE);
                    }

                }else{

                    if (feed_item_list.size()<1){
                        emty_relative.setVisibility(View.VISIBLE);
                    }
                }
            }

            @Override
            public void onFailure(retrofit2.Call<GlobalScoreModel> call, Throwable t) {
                if (feed_item_list.size()<1){
                    emty_relative.setVisibility(View.VISIBLE);
                }
            }
        });

    }
}
