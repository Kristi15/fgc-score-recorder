package first.global.fgcscore.Fragment;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.greenrobot.eventbus.EventBus;

import first.global.fgcscore.MainActivity;
import first.global.fgcscore.Model.MessageModel;
import first.global.fgcscore.R;
import first.global.fgcscore.Rest.API;
import first.global.fgcscore.Rest.ApiClient;
import first.global.fgcscore.Utils.EventSender;
import first.global.fgcscore.Utils.SaveData;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegisterFragment extends Fragment {

    EditText name_et,username_et,password_et;
    Button register_button;
    Gson gson;
    SaveData saveData;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v =inflater.inflate(R.layout.fragment_register, container, false);
        initsview(v);



        return v;
    }



    public void initsview(View v){
        saveData = new SaveData(getActivity());
        gson = new GsonBuilder().create();


        name_et = (EditText) v.findViewById(R.id.country_name_et_register);
        username_et = (EditText)v.findViewById(R.id.username_et_register);
        password_et = (EditText) v.findViewById(R.id.password_et_register);
        register_button = (Button) v.findViewById(R.id.register_button);

        register_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!name_et.getText().toString().trim().equalsIgnoreCase("")&&
                        !username_et.getText().toString().trim().equalsIgnoreCase("")||
                        !password_et.getText().toString().trim().equalsIgnoreCase("")) {
                    register_call(name_et.getText().toString(),
                            username_et.getText().toString(),
                            password_et.getText().toString());

                }
                if (username_et.getText().toString().equalsIgnoreCase("")){
                    YoYo.with(Techniques.Tada)
                            .duration(700)
                            .repeat(0)
                            .playOn(username_et);

                }
                if (name_et.getText().toString().equalsIgnoreCase("")){
                    YoYo.with(Techniques.Tada)
                            .duration(700)
                            .repeat(0)
                            .playOn(name_et);

                }
                if (password_et.getText().toString().equalsIgnoreCase("")){
                    YoYo.with(Techniques.Tada)
                            .duration(700)
                            .repeat(0)
                            .playOn(password_et);

                }
            }
        });




    }



    public void register_call(final String name, final String username, String password){
        API apiclient = ApiClient.createAPI_No_Token();
        Call<MessageModel> call  = apiclient.register(name,username,password);
        call.enqueue(new Callback<MessageModel>() {
            @Override
            public void onResponse(Call<MessageModel> call, Response<MessageModel> response) {
                if (!gson.toJson(response.body()).equalsIgnoreCase("null")){
                    if (!response.body().getMessage().equalsIgnoreCase("fail")){

                    saveData.saveuserinfo(name,username);
                    saveData.setguest(false);
                    saveData.saveScore("0");
                        EventBus.getDefault().post(new EventSender("new_score%"+saveData.getScore()));

                        startActivity(new Intent(getActivity(),MainActivity.class));

                    }
                    else
                        Toast.makeText(getActivity(), "Country Already Registered. Sign In or Contact us for Help!", Toast.LENGTH_LONG).show();
                }else
                    Toast.makeText(getActivity(), "Server is having some issues. Try Again Later!", Toast.LENGTH_LONG).show();
            }

            @Override
            public void onFailure(Call<MessageModel> call, Throwable t) {

            }
        });

    }

}
