package first.global.fgcscore.Activities;

import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.codehaus.jackson.map.DeserializationConfig;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import first.global.fgcscore.Adapter.ScoreAdapter;
import first.global.fgcscore.Fragment.Fragment_Global_ScoreBoard;
import first.global.fgcscore.Fragment.Fragment_Local_ScoreBoard;
import first.global.fgcscore.Model.ScoreTeamModel;
import first.global.fgcscore.R;
import first.global.fgcscore.Utils.SaveData;

public class ScoreListActivity extends AppCompatActivity {
    ViewPager mViewPager;
    Fragment_Global_ScoreBoard f_global;
    Fragment_Local_ScoreBoard f_local;
    ImageView back_button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        setContentView(R.layout.activity_score_list);

        mViewPager = (ViewPager) findViewById(R.id.viewpager);
        back_button = (ImageView) findViewById(R.id.back_btn);
        back_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        TabLayout tabLayout = (TabLayout) findViewById(R.id.tablayout);

        setupViewPager(mViewPager);
        tabLayout.setupWithViewPager(mViewPager);

    }
    private void setupViewPager(ViewPager mViewPager) {
        MyPagerAdapter adapter = new MyPagerAdapter(getSupportFragmentManager());
        if( f_local == null ){ f_local = new Fragment_Local_ScoreBoard(); }
        if( f_global == null ){ f_global = new Fragment_Global_ScoreBoard(); }
        adapter.addFragment(f_local, "Local Score");
        adapter.addFragment(f_global, "Global Score");
        mViewPager.setAdapter(adapter);
    }


    static class MyPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragments = new ArrayList<>();
        private final List<String> mFragmentTitles = new ArrayList<>();
        public MyPagerAdapter(FragmentManager fm) {
            super(fm);
        }
        public void addFragment(Fragment fragment, String title) {
            mFragments.add(fragment);
            mFragmentTitles.add(title);
        }
        @Override
        public Fragment getItem(int position) {
            return mFragments.get(position);
        }
        @Override
        public int getCount() {
            return mFragments.size();
        }
        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitles.get(position);
        }
    }


}
