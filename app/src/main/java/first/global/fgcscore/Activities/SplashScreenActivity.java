package first.global.fgcscore.Activities;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;

import java.util.ArrayList;
import java.util.List;

import first.global.fgcscore.MainActivity;
import first.global.fgcscore.Model.ScoreTeamModel;
import first.global.fgcscore.R;
import first.global.fgcscore.Utils.SaveData;

public class SplashScreenActivity extends AppCompatActivity {
    ActionBar actionBar;
    SaveData saveData;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_splash_screen);
        saveData = new SaveData(getApplicationContext());
        actionBar = getSupportActionBar();
        actionBar.hide();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                gotomenu();
            }
        },2000);

    }
    public void gotomenu(){
        Intent intent = new Intent(SplashScreenActivity.this,MainActivity.class);
        startActivity(intent);
        finish();
    }
}
