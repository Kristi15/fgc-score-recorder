package first.global.fgcscore.Activities;

import android.graphics.Color;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import org.greenrobot.eventbus.EventBus;

import first.global.fgcscore.Fragment.LoginFragment;
import first.global.fgcscore.Fragment.RegisterFragment;
import first.global.fgcscore.R;
import first.global.fgcscore.Utils.EventSender;
import first.global.fgcscore.Utils.SaveData;

public class LoginRegisterActivity extends AppCompatActivity {
    SaveData saveData;
    ActionBar actionBar;
    LoginFragment f_login;
    RegisterFragment f_register;


    ImageView close_activity;
    Boolean is_login = true;
    TextView loginregisterswitcher;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        actionBar = getSupportActionBar();
        actionBar.hide();
        Window window = getWindow();
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.setStatusBarColor(Color.WHITE);
        setContentView(R.layout.activity_login_register);
        saveData = new SaveData(getApplicationContext());

        loginregisterswitcher = (TextView) findViewById(R.id.login_register_swicher);
        close_activity = (ImageView) findViewById(R.id.close_imageview);
        close_activity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        loginregisterswitcher.setText(Html.fromHtml("Don't have an account yet?<br><b>Register</b>"));
         loginregisterswitcher.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View v) {

                 if (is_login){
                     loginregisterswitcher.setText(Html.fromHtml("<span>Already have an account?<br></span><b>Login</b"));
                     f_register();
                     is_login = false;
                 }else{
                     loginregisterswitcher.setText(Html.fromHtml("<span>Don't have an account yet?<br></span><b>Register</b>"));
                     f_login();
                     is_login = true;
                 }



             }
         });

         f_starter();


    }


    public void f_starter() {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        f_login  = new LoginFragment();
        fragmentTransaction.add(R.id.user_auth_framelayout, f_login, "Sherbime");
        fragmentTransaction.commit();
        actionBar.setTitle("Data");
    }
    public void f_login() {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        f_login = new LoginFragment();
        fragmentTransaction.replace(R.id.user_auth_framelayout, f_login , "Rezervime");
        fragmentTransaction.commit();
        actionBar.setTitle("Data");

    }
    public void f_register() {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        f_register  = new RegisterFragment();
        fragmentTransaction.replace(R.id.user_auth_framelayout, f_register, "Rezervime");
        fragmentTransaction.commit();
        actionBar.setTitle("Ora");

    }



    public void testlogin(){

        saveData.setguest(false);
        saveData.saveuserinfo("Albania","Albania2018");
        EventBus.getDefault().post(new EventSender("signed_in"));
    }
}
