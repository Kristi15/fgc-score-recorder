package first.global.fgcscore.Activities;

import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.drawable.ColorDrawable;
import android.os.CountDownTimer;
import android.os.Handler;
import android.renderscript.Sampler;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Switch;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.codehaus.jackson.map.DeserializationConfig;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;
import org.greenrobot.eventbus.EventBus;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.concurrent.TimeUnit;

import first.global.fgcscore.Model.MessageModel;
import first.global.fgcscore.Model.ScoreTeamModel;
import first.global.fgcscore.R;
import first.global.fgcscore.Rest.API;
import first.global.fgcscore.Rest.ApiClient;
import first.global.fgcscore.Utils.EventSender;
import first.global.fgcscore.Utils.SaveData;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class GameActivity extends AppCompatActivity  {
    TextView before_starting_counter,time_counting,point_counting,solar_number,combusion_low,combusion_high,parked_robot_quantity;
    RelativeLayout loading_relative,actionbar;
LinearLayout main_game;
    Boolean is_wind_turbine_on = false,is_combotion_plant_on = false;
    Switch wind_turbine,combotion_plant_switch,parked_robot,linked_village;
    int score,wind_score;
    Button increase_solars,btn_high_plus,btn_low_plus,btn_high_minus,btn_low_minus,parked_robot_increase;
    SaveData saveData;
    Dialog dialog;
    Boolean is_Exit = false;
    Gson gson;

    @Override
    protected void onCreate(Bundle savedInstanceState) {


        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);
gson = new GsonBuilder().create();
        saveData = new SaveData(getApplicationContext());
        wind_turbine   = (Switch) findViewById(R.id.wind_turbine);
        combotion_plant_switch   = (Switch) findViewById(R.id.combotion_plant_switch);
        actionbar = (RelativeLayout) findViewById(R.id.actionbar);
        main_game = (LinearLayout) findViewById(R.id.main_game);
        time_counting = (TextView) findViewById(R.id.time_counting);
        point_counting = (TextView) findViewById(R.id.point_counting);
        solar_number = (TextView) findViewById(R.id.solar_number);
        combusion_low = (TextView) findViewById(R.id.low_quantity);
        combusion_high = (TextView) findViewById(R.id.high_quantity);
        parked_robot_quantity = (TextView) findViewById(R.id.pareked_robot_qunatity);
        parked_robot_increase = (Button) findViewById(R.id.parked_robot_btn);
        combusion_high.setText("0");
        combusion_low.setText("0");
        increase_solars = (Button) findViewById(R.id.increase_solars);
        increase_solars.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!solar_number.getText().toString().equalsIgnoreCase("5")){
                    solar_number.setText(Integer.valueOf(solar_number.getText().toString()) + 1+ "");
                }
            }
        });
        actionbar.setVisibility(View.GONE);
        wind_turbine.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    is_wind_turbine_on = true;

                    wind_turbine.setEnabled(false);
                }
            }
        });combotion_plant_switch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    is_combotion_plant_on = true;

                    combotion_plant_switch.setEnabled(false);
                }
            }
        });

        btn_high_minus = (Button) findViewById(R.id.btn_high_minus);
        btn_high_plus = (Button) findViewById(R.id.btn_high_plus);
        btn_low_plus = (Button) findViewById(R.id.btn_low_plus);
        btn_low_minus = (Button) findViewById(R.id.btn_low_minus);

        btn_high_plus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                combusion_high.setText(Integer.parseInt(combusion_high.getText().toString())+ 1+"");
                score = score+20;

            }
        });
        btn_high_minus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (combusion_high.getText().toString().equalsIgnoreCase("0")){

                }else{
                    combusion_high.setText(Integer.parseInt(combusion_high.getText().toString())- 1+"");
                    score = score-20;
                }

            }
        });





        btn_low_plus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                combusion_low.setText(Integer.parseInt(combusion_low.getText().toString())+1+"");
                score = score+5;

            }
        });
        btn_low_minus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (combusion_low.getText().toString().equalsIgnoreCase("0")){

                }else{
                    combusion_low.setText(Integer.parseInt(combusion_low.getText().toString())- 1+"");
                    score = score-5;
                }
            }
        });


        linked_village = (Switch) findViewById(R.id.link_with_other_team);


        parked_robot_increase.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (parked_robot_quantity.getText().toString().equalsIgnoreCase("2")){
                    parked_robot_quantity.setText(Integer.valueOf(parked_robot_quantity.getText().toString())+ 1+"");
                    score  = score  + 20;
                }else if (parked_robot_quantity.getText().toString().equalsIgnoreCase("3")){

                }else{
                    parked_robot_quantity.setText(Integer.valueOf(parked_robot_quantity.getText().toString())+ 1+"");
                    score  = score + 15;
                }
            }
        });


        linked_village.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked){
                    score +=100;
                    linked_village.setEnabled(false);
                }
            }
        });
        starting();








    }


    public void starting(){
        getSupportActionBar().hide();
        before_starting_counter = (TextView) findViewById(R.id.counter);
        loading_relative = (RelativeLayout) findViewById(R.id.starting_relative);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                before_starting_counter.setText(2+"");
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        before_starting_counter.setText(1+"");
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {

                                before_starting_counter.setText(0+"");
                                loading_relative.setVisibility(View.GONE);

                                Window window = GameActivity.this.getWindow();
                                window.setFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN,
                                        WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN);
                                actionbar.setVisibility(View.VISIBLE);
                                main_game.setVisibility(View.VISIBLE);
                                startCouting();

                            }
                        },1000);
                    }
                },1000);

            }
        },1000);


    }
    public void startCouting(){
        new CountDownTimer(150000, 1000) { // adjust the milli seconds here

            public void onTick(long millisUntilFinished) {
                time_counting.setText(""+String.format("%d:%d sec",
                        TimeUnit.MILLISECONDS.toMinutes( millisUntilFinished),
                        TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) -
                                TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished))));
                if (time_counting.getText().toString().equalsIgnoreCase("0:30 sec")){
                    time_counting.setTextColor(getResources().getColor(android.R.color.holo_red_dark));
                }


                if(is_wind_turbine_on){
                score = score +1;
                }

                if(is_combotion_plant_on){
                score = score +3;
                }
                score = score+ Integer.valueOf(solar_number.getText().toString());
                point_counting.setText("Score: " + score + " points" );




            }

            public void onFinish() {
        if (is_Exit){

        }else{
            try{
                dialog = new Dialog(GameActivity.this);
                dialog.setContentView(R.layout.dialog_end_game_score);

                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                showDialog();
            }catch (Exception e){

            }

        }
            }
        }.start();
    }

    @Override
    public void onBackPressed() {
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which){
                    case DialogInterface.BUTTON_POSITIVE:
                            finish();
                            is_Exit = true;
                        break;

                    case DialogInterface.BUTTON_NEGATIVE:

                        break;
                }
            }
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(GameActivity.this);
        builder.setMessage(Html.fromHtml("Are you sure you want to <span style='text-size:30px; color: red;'>LEAVE?</span>"))
                .setPositiveButton(Html.fromHtml(" <span style='color: red;'>Yes</span>"), dialogClickListener)
                .setNegativeButton("No", dialogClickListener).show();
    }
    public void showDialog(){



        if (Integer.valueOf(saveData.getScore())>score){

        }else{
            savescoreonlineifnotguest();
        }

        dialog.setCancelable(false);
        dialog.show();

        Button menu_btn = (Button) dialog.findViewById(R.id.menu_btn);
        Button reset_btn = (Button) dialog.findViewById(R.id.reset_btn);
        Button save_btn = (Button) dialog.findViewById(R.id.save_btn);
        reset_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                startActivity(getIntent());
                finish();
            }
        });

        menu_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                finish();
            }
        });
        save_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Integer.valueOf(saveData.getScore())>score){

                }else{

                    saveData.saveScore(score+"");

                    EventBus.getDefault().post(new EventSender("new_score%"+score));

                }

                Toast.makeText(GameActivity.this, "Score Stored", Toast.LENGTH_SHORT).show();


                ObjectMapper mapper = new ObjectMapper();
                mapper.configure(DeserializationConfig.Feature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true);
                mapper.configure(DeserializationConfig.Feature.FAIL_ON_UNKNOWN_PROPERTIES, false);
                List<ScoreTeamModel> feed_item_list = new ArrayList<>();
               if (!saveData.get_list_score().equalsIgnoreCase("")){

                try {
                    feed_item_list = mapper.readValue(saveData.get_list_score(), new TypeReference<List<ScoreTeamModel>>() {
                    });
                    // Toast.makeText(this, "try", Toast.LENGTH_SHORT).show();
                } catch (IOException e) {
                    e.printStackTrace();
                    // Toast.makeText(this, "catch", Toast.LENGTH_SHORT).show();
                }}

                ScoreTeamModel test = new ScoreTeamModel();
                test.setId(feed_item_list.size()+"");
                test.setScore(score+"");
                String year  = Calendar.getInstance().getTime().getYear()+ "";
                year = "20" + year.substring(1,3);

                String date  =Calendar.getInstance().getTime().getDate() + "/"+
                        Calendar.getInstance().getTime().getMonth() + "/"+year;
                test.setDate(date);
                feed_item_list.add(test);

                System.out.println("feedotem s" + gson.toJson(feed_item_list.toString()));
                saveData.save_list_score(gson.toJson(feed_item_list));


            }
        });

        TextView score_tx = (TextView) dialog.findViewById(R.id.score_text);
        score_tx.setText("Your Score is : " + score + " Points");



    }

    private void savescoreonlineifnotguest() {

        if (saveData.isguest()){

        }else{

            API apiCLient = ApiClient.createAPI_No_Token();
            retrofit2.Call<MessageModel> call = apiCLient.setScore(saveData.get_country_name(),saveData.get_country_username(),score+"");
            call.enqueue(new Callback<MessageModel>() {
                @Override
                public void onResponse(Call<MessageModel> call, Response<MessageModel> response) {
                    if (!gson.toJson(response.body()).equalsIgnoreCase("null")){

                    }else{

                    }
                }

                @Override
                public void onFailure(Call<MessageModel> call, Throwable t) {

                }
            });





        }

    }


}
