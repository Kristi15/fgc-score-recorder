package first.global.fgcscore.Adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import first.global.fgcscore.Model.Data.GlobalScoreDataModel;
import first.global.fgcscore.Model.ScoreTeamModel;
import first.global.fgcscore.R;
import first.global.fgcscore.Utils.ItemClickListener;

public class GlobalScoreAdapter extends RecyclerView.Adapter<GlobalScoreAdapter.ViewHolder> {

    public static List<GlobalScoreDataModel> feedItemList,filterList;
    private Context mContext;
    int id_category;
    private Activity gallery_activity;
    int i;
    /**
     * The system "short" animation time duration, in milliseconds. This duration is ideal for
     * subtle animations or animations that occur very frequently.
     */

    public GlobalScoreAdapter(Context context, Activity activity, List<GlobalScoreDataModel> feedItemList) {
        this.feedItemList = feedItemList;
        this.mContext = context;
        this.gallery_activity = activity;
        this.filterList=feedItemList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View itemView = inflater.inflate(R.layout.score_list_structure, parent, false);
        return new ViewHolder(itemView);
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {


        final GlobalScoreDataModel feedItem = feedItemList.get(position);

        holder.setClickListener(new ItemClickListener() {
            @Override
            public void onClick(View view, int position, boolean isLongClick) {
                if (isLongClick) {

                } else {



                }

            }




        });



        holder.id.setText(position+1+"");
        holder.date.setText("Country: "  +feedItem.getCountryName());
        holder.score.setText(feedItem.getCountryScore()+ " points");


    }



    @Override
    public int getItemCount() {
        return (null != feedItemList ? feedItemList.size() : 0);
    }



    public static class ViewHolder extends RecyclerView.ViewHolder
            implements View.OnClickListener, View.OnLongClickListener {


        private ItemClickListener clickListener;
        TextView id,score,date;
        public ViewHolder(View itemView) {
            super(itemView);

            id  = itemView.findViewById(R.id.id);
            score = itemView.findViewById(R.id.score);
            date  = itemView.findViewById(R.id.date);

        }

        public void setClickListener(ItemClickListener itemClickListener) {
            this.clickListener = itemClickListener;
        }

        @Override
        public void onClick(View view) {
            clickListener.onClick(view, getPosition(), false);
        }

        @Override
        public boolean onLongClick(View view) {
            clickListener.onClick(view, getPosition(), true);
            return true;
        }
    }
  
}