package first.global.fgcscore.Rest;

import first.global.fgcscore.Model.GlobalScoreModel;
import first.global.fgcscore.Model.MessageModel;
import first.global.fgcscore.Model.SignInModel;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface API {

    String BASE_URL = "https://fgcscorer.theglobalalliance.net/";

    @GET("api.php?a=getscores")
    Call<GlobalScoreModel> get_globalScores();

    @FormUrlEncoded
    @POST("api.php?a=setscore")
    Call<MessageModel> setScore(@Field("name") String name,
                                @Field("username") String username,
                                @Field("score") String score);

    @FormUrlEncoded
    @POST("api.php?a=signup")
    Call<MessageModel> register(@Field("name") String name,
                                @Field("username") String username,
                                @Field("password") String score);

    @FormUrlEncoded
    @POST("api.php?a=signin")
    Call<SignInModel> login(    @Field("username") String username,
                               @Field("password") String password);
}
