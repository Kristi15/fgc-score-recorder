package first.global.fgcscore;

import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.CountDownTimer;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.QuickContactBadge;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.Timer;
import java.util.concurrent.TimeUnit;

import first.global.fgcscore.Activities.GameActivity;
import first.global.fgcscore.Activities.LoginRegisterActivity;
import first.global.fgcscore.Activities.ScoreListActivity;
import first.global.fgcscore.Utils.EventSender;
import first.global.fgcscore.Utils.SaveData;

public class MainActivity extends AppCompatActivity {
    LinearLayout start_game,leaderboard;
    Button sign_in;
    RelativeLayout signed_in,guest_info;
    TextView score_points,hello_txt,points_as_guest;
    Boolean is_clicked_back = false;
    SaveData saveData;
    EventBus eventBus;
    ImageView log_out;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getSupportActionBar().hide();
        Window window = getWindow();
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.setStatusBarColor(Color.WHITE);
        eventBus.getDefault();
        EventBus.getDefault().unregister(this);
        EventBus.getDefault().register(this);
        saveData = new SaveData(getApplicationContext());
        start_game  = (LinearLayout) findViewById(R.id.start_game_linear);
        leaderboard  = (LinearLayout) findViewById(R.id.leader_board_linear);
        sign_in =(Button)findViewById(R.id.signin_team);
        signed_in =(RelativeLayout) findViewById(R.id.logged_in);
        guest_info =(RelativeLayout) findViewById(R.id.guest_info);
        score_points =(TextView) findViewById(R.id.score_points);
        hello_txt =(TextView) findViewById(R.id.hello_text);
        points_as_guest =(TextView) findViewById(R.id.points_as_guest);
        log_out =(ImageView) findViewById(R.id.log_out);

        log_out.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which){
                            case DialogInterface.BUTTON_POSITIVE:
                                saveData.clearAll();
                                startActivity(getIntent());
                                finish();
                                break;

                            case DialogInterface.BUTTON_NEGATIVE:
                                //No button clicked
                                break;
                        }
                    }
                };

                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                builder.setMessage("Are you sure you want to Log Out?").setPositiveButton("Yes", dialogClickListener)
                        .setNegativeButton("No", dialogClickListener).show();
            }
        });

        score_points.setText("Your Best Score was: " + saveData.getScore() + " Points");

        if(saveData.isguest()){
            points_as_guest.setText("Current Record: " + saveData.getScore()+ " Points");
        }else{
            guest_info.setVisibility(View.GONE);
            sign_in.setVisibility(View.GONE);
            signed_in.setVisibility(View.VISIBLE);

            if(saveData.get_country_name().toLowerCase().contains("team")){
                hello_txt.setText("Hi "+ saveData.get_country_name());
            }else
                hello_txt.setText("Hi Team "+ saveData.get_country_name());



        }


        sign_in.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, LoginRegisterActivity.class);
                startActivity(intent);

            }
        });

        start_game.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, GameActivity.class);
                startActivity(intent);
                start_game.setClickable(false);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        start_game.setClickable(true);
                    }
                },2000);
            }
        });
        leaderboard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, ScoreListActivity.class);
                startActivity(intent);
                leaderboard.setClickable(false);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        leaderboard.setClickable(true);
                    }
                },2000);
            }
        });

    }

    @Override
    public void onBackPressed() {
        if (is_clicked_back){
            finish();
        }else{
            Toast.makeText(this, "Press back again to exit the ap!", Toast.LENGTH_SHORT).show();
            is_clicked_back = true;
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    is_clicked_back = false;
                }
            },2500);
        }



    }

    @SuppressLint("SetTextI18n")
    @Subscribe
    public void onEvent(EventSender event){
            if (event.getEvent().startsWith("new_score")){

                score_points.setText("Your Best Score was: " + event.getEvent().split("%")[1] + " Points");
                points_as_guest.setText("Current Record: " + saveData.getScore()+ " Points");

            }

            if (event.getEvent().startsWith("signed_in")){
                if(saveData.isguest()){

                }else{
                    sign_in.setVisibility(View.GONE);
                    signed_in.setVisibility(View.VISIBLE);
                    if(saveData.get_country_name().toLowerCase().contains("team")){

                        hello_txt.setText("Hi "+ saveData.get_country_name());
                    }else
                    hello_txt.setText("Hi Team "+ saveData.get_country_name());


                }



            }
    }
}
