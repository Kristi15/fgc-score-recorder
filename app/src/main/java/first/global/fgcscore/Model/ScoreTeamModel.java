package first.global.fgcscore.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.Date;

public class ScoreTeamModel {

    @SerializedName("id")
    @Expose
    public String id;
    @SerializedName("score")
    @Expose
    public String score;
    @SerializedName("date")
    @Expose
    public String date;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getScore() {
        return score;
    }

    public void setScore(String score) {
        this.score = score;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }


}