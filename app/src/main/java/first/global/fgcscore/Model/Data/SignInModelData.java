package first.global.fgcscore.Model.Data;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SignInModelData {
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("country_username")
    @Expose
    private String countryUsername;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCountryUsername() {
        return countryUsername;
    }

    public void setCountryUsername(String countryUsername) {
        this.countryUsername = countryUsername;
    }

}
