package first.global.fgcscore.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import first.global.fgcscore.Model.Data.GlobalScoreDataModel;

public class GlobalScoreModel {

    @SerializedName("data")
    @Expose
    private List<GlobalScoreDataModel> data = null;

    public List<GlobalScoreDataModel> getData() {
        return data;
    }

    public void setData(List<GlobalScoreDataModel> data) {
        this.data = data;
    }
}
