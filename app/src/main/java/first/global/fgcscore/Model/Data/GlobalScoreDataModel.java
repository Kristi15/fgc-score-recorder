package first.global.fgcscore.Model.Data;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GlobalScoreDataModel {

    @SerializedName("country_name")
    @Expose
    private String countryName;
    @SerializedName("country_score")
    @Expose
    private String countryScore;

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public String getCountryScore() {
        return countryScore;
    }

    public void setCountryScore(String countryScore) {
        this.countryScore = countryScore;
    }
}
