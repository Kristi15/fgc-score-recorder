package first.global.fgcscore.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import first.global.fgcscore.Model.Data.SignInModelData;

public class SignInModel {

    @SerializedName("data")
    @Expose
    private SignInModelData data;
    @SerializedName("message")
    @Expose
    private String message;

    public SignInModelData getData() {
        return data;
    }

    public void setData(SignInModelData data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}
